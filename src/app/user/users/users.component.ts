import { Component, OnInit } from '@angular/core';

import { User } from '../user';
import { Page } from '../../shared/page';
import { UserService } from '../user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[];
  page: Page = {
    page: 1,
    limit: 5,
    skip: 0,
    total: 0  
  };

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  setPage(page){
    this.page.page = page.offset+1;
    this.page.skip = Math.floor(this.page.page/this.page.total);
    this.getUsers();
  }

  getUsers() {
    let res = this.userService.getUsers((this.page.page*this.page.limit)-this.page.limit, this.page.limit);
    this.page.total =  res.total;
    this.users = res.data;
  }
  
}
