import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from './shared/material.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { AppComponent } from './app.component';
import { UsersComponent } from './user/users/users.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { WebixComponent } from './webix/webix.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    WelcomeComponent,
    WebixComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    NgxDatatableModule,
    RouterModule.forRoot([
      { path: '', component: WelcomeComponent },
      { path: 'users', component: UsersComponent },
      { path: 'webix', component: WebixComponent },
      { path: '**', redirectTo:'' }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
