import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

declare var webix: any;

@Component({
  selector: 'app-webix',
  templateUrl: './webix.component.html',
  styleUrls: ['./webix.component.css']
})
export class WebixComponent implements OnInit {
  @ViewChild('webixContainer') webixContainer: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;
  books: any[];
  writers: any[];
  writersAndBooks: any[];
  tree:any;
  fileAddToggle: boolean = false;

  constructor() {
    this.books=[
      {id:0, title: 'The Great Gatsby', writer: 'F. Scott Fitzgerald'},
      {id:1, title: 'The Short Stories of F. Scott Fitzgerald', writer: 'F. Scott Fitzgerald'},
      {id:2, title: 'Winter Dreams', writer: 'F. Scott Fitzgerald'},
      {id:3, title: 'Nineteen Eighty-Four', writer: 'George Orwell'},
      {id:4, title: 'Homage to Catalonia', writer: 'George Orwell'},
      {id:5, title: 'Down and Out in Paris and London', writer: 'George Orwell'},
    ];
    this.writers=[
      {name: 'F. Scott Fitzgerald'},
      {name: 'George Orwell'}
    ];
    this.getWritersAndBooks();
  }

  ngOnInit() {
    this.tree = webix.ui({
      view:"tree",
      container: this.webixContainer.nativeElement, 
      select:true,
      drag:true,
      on:{
        onAfterSelect:($e)=>{this.tree.adjust()},
        onAfterOpen: ($e)=>{this.tree.adjust()}
      },
      data: this.feedTree()
      });
  }

  addFile(){
    if(this.fileAddToggle){
      let parent = this.tree.getSelectedId() ? this.tree.getSelectedId() : 0 ; 
      if((this.fileInput.nativeElement.value).trim() == '') return false;
      this.tree.add({id: this.fileInput.nativeElement.value, value: this.fileInput.nativeElement.value}, 0, parent)
    }else{
      this.fileAddToggle = !this.fileAddToggle;
      return;
    }
    this.resetField();
    this.fileAddToggle = !this.fileAddToggle;
  }

  private resetField(){
    this.fileInput.nativeElement.value = '';
  }

  removeFile(){
    if(!this.tree.getSelectedId()) return;
    let isBranch = this.tree.isBranch(this.tree.getSelectedId());
    if(isBranch){
      let childIds = [];
      this.tree.data.eachLeaf(this.tree.getSelectedId(), (child)=>{
        childIds.push(child.id)
      })
      childIds.forEach(r=>{
        this.tree.move(r, 0, 0)
      })
      this.tree.remove(this.tree.getSelectedId());
    }else{
      this.tree.remove(this.tree.getSelectedId());
    }
  }

  getWritersAndBooks(){
    // Combine books and writers
    let writersBooks=[];
    this.writers.forEach(writer=>{
      writer.books = [];
      this.books.forEach(book=>{
        if(writer.name == book.writer){
          writer.books.push(book);
        }
      })
      writersBooks.push(writer);
    })
    this.writersAndBooks = writersBooks;
  }

  feedTree(){
    let tree:any[] = [];
    let i = 1;
    this.writersAndBooks.forEach(writer=>{
      let branch: any={};
      branch.id = i; i++;
      // console.log(branch)
      branch.value = writer.name;
      writer.books.forEach(book=>{
        book.id = book.title;
        book.value = book.title;
      })
      branch.data = writer.books;
      tree.push(branch);
    })
    return tree;
  }
}
