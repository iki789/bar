import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebixComponent } from './webix.component';

describe('WebixComponent', () => {
  let component: WebixComponent;
  let fixture: ComponentFixture<WebixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
