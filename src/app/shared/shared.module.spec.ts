import { MaterialModule } from './material.module';

describe('SharedModule', () => {
  let sharedModule: MaterialModule;

  beforeEach(() => {
    sharedModule = new MaterialModule();
  });

  it('should create an instance', () => {
    expect(sharedModule).toBeTruthy();
  });
});
