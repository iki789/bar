export interface Page{
  limit: number,
  page: number,
  skip: number,
  total?: number
}